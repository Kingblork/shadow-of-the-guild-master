﻿using UnityEngine;
using System.Collections;

public class sphereAttack : MonoBehaviour {

	public int dmg = 100;
	
	void OnTriggerEnter2D(Collider2D col){
		if(col.isTrigger == false && col.CompareTag("Ennemi")){
			col.SendMessageUpwards("Dommage",dmg);
		}
	}
}
