﻿using UnityEngine;
using System.Collections;

public class PersoController : MonoBehaviour {
	
	public float vitesse = 4f;
	public float rayonSol = 0.05f;

	public bool toucheSol = true;
	public bool grimpe = false;
	public float grimpeY = 0;
	public float grimpeX = 0;
	public float grimpeDelay = 0.15f;
	public float direction = 1;
	public float health = 1000;
	public bool hitbyMVilain = false;
	public bool baisser = false;

	public LayerMask Sol;
	public Transform checkSol;
	public Animator anim;
	public Rigidbody2D rigid;



	private Transform transformCache;
	
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		rigid = GetComponent<Rigidbody2D> ();
	}

	void Awake () {
		transformCache = transform;
	}
	
	void FixedUpdate(){
		toucheSol = Physics2D.OverlapCircle (checkSol.position, rayonSol,Sol);

		anim.SetBool ("Sol",toucheSol);

	}
	
	// Update is called once per frame
	void Update () {

		if (toucheSol == true && Input.GetKey (KeyCode.DownArrow)) {
			anim.Play ("Baisser");
			baisser = true;
		}

		if (toucheSol == true && Input.GetKeyUp(KeyCode.DownArrow)) {
			anim.Play ("Statique");
			baisser = false;
		}

		rigid.MoveRotation (0);
		float x = Input.GetAxis ("Horizontal");



		if (x > 0 && baisser == false) {
			direction = x;
			transformCache.Translate(x*Time.deltaTime*vitesse,0,0);
			transformCache.eulerAngles = new Vector2 (0,0);
			anim.SetFloat ("Vitesse", Mathf.Abs (x));
		}
		
		if (x < 0 && baisser == false) {
			direction = x;
			transformCache.Translate(-x*Time.deltaTime*vitesse,0,0);
			transformCache.eulerAngles = new Vector2 (0,180);
			anim.SetFloat ("Vitesse", Mathf.Abs (x));	
		}

		if (toucheSol == true && Input.GetButtonDown ("Jump")) {
			rigid.AddForce (new Vector2 (1, 200));
			anim.SetBool ("Saut", true);
		} else {
			anim.SetBool ("Saut", false);
		}


		if (toucheSol == true) {
			vitesse = 4f;
		} else {
			vitesse = 2f;
		}


		if (grimpe == true && grimpeX < 0.5f && grimpeY < 0.5f) {
			if(grimpeDelay < 0){

					rigid.mass = 1;
					transformCache.Translate (1 * 0.15f, 1 * 0.15f, 0);

				grimpeX += 0.2f;
				grimpeY += 0.2f;

			}else{
				rigid.mass = 0;
				grimpeDelay -= Time.deltaTime;
			}
		} else {
			grimpeX = 0;
			grimpeY = 0;
			grimpe = false;
			grimpeDelay = 0.25f;
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.isTrigger == true && col.CompareTag("rebord"))
		{
			grimpe = true;
			anim.Play("grimpe"); 
		}
	}

	public void Dommage(int dmg){
		health -= dmg;
		anim.Play("getCoup");
		hitbyMVilain = true;
	}
}
