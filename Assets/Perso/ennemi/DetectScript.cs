﻿using UnityEngine;
using System.Collections;

public class DetectScript : MonoBehaviour {

	private MechantScript[] scriptParent;
	private Collider2D coll;

	void Awake (){
		scriptParent = this.GetComponentsInParent<MechantScript>();
	}

	void OnTriggerEnter2D(Collider2D col){
		coll = col;
		if(col.isTrigger == true && col.CompareTag("DetectPerso")){
			scriptParent[0].detect=true;
		}
	}

	void OnTriggerExit2D(Collider2D col){
		//if (coll.isTrigger == true && coll.CompareTag ("DetectPerso")) {
			scriptParent [0].detect = false;
		//}
	}

	
}