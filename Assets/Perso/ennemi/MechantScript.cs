﻿using UnityEngine;
using System.Collections;


public class MechantScript : MonoBehaviour {
	//
	public int health = 200;
	public bool detect = false;
	public bool canAttack = false;
	//
	public int distanceDeplacement = 0;
	public bool changeDirection = false;
	//
	public float delayBeforeAttack = 3;
	//
	private Animator anim;
	private int delay = 2; 
	private Transform TransformMe;
    private PlayerLife life;

	// Use this for initialization
	void Start () {
		//Récupération de l'animator
		anim = GetComponent<Animator> ();
        life = GetComponent<PlayerLife>();
        life.maxHealth = health;
    }

	void Awake (){
		TransformMe = transform;
	}
	
	// Update is called once per frame
	void Update () {

		delayBeforeAttack -= Time.deltaTime;

		if (delayBeforeAttack < 0) {
			canAttack = true;
		}

		//Si la vie est inférieur ou égale à 0
		if (health <= 0) {
			//Appel de la fonction de destruction de l'objet
			anim.Play("DieDra");
			GameObject.Destroy(gameObject,0.3f);
			
		}
		//Si le méchant ne détecte pas le personnage alors on joue son role de déplacement classique
		if (detect == false) {
			if (changeDirection == false) {
				distanceDeplacement++;
				TransformMe.Translate (new Vector3 (5 * Time.deltaTime, 0, 0));
				TransformMe.eulerAngles = new Vector2 (0, 0);
				anim.SetBool ("Deplace", true);
			} else {
				distanceDeplacement--;
				TransformMe.eulerAngles = new Vector2 (0, 180);
				TransformMe.Translate (new Vector3 (5* Time.deltaTime, 0, 0));
				anim.SetBool ("Deplace", true);
			}

			if (distanceDeplacement < 0) {
				changeDirection = false;
			}
			if (distanceDeplacement > 200) {
				changeDirection = true;
			}

		} else {
			anim.SetBool ("Deplace", false);
				if(canAttack == true){
					anim.Play("hit1Dra");
					canAttack = false;
					delayBeforeAttack = Random.Range(0,5);
				}

		}
	}
	

	public void MoveAndAttackPerso(bool valeur){
		detect = valeur;

	}

	public void Dommage(int dmg){
		health -= dmg;
        life.TakeDamage(dmg);
		anim.Play("AttackBy");
		delayBeforeAttack += 1;
	}
	
}
