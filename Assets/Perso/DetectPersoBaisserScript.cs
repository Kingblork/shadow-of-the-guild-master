﻿using UnityEngine;
using System.Collections;

public class DetectPersoBaisserScript : MonoBehaviour {
	
	public Collider2D coll;
	public PersoController pers;
	
	void Awake(){
		
		
		coll = GetComponent<Collider2D> ();
	}
	
	public void Hide(bool valeur){
		coll.enabled = valeur;
	}
	
	
}