﻿using UnityEngine;
using System.Collections;

public class boltScript : MonoBehaviour {

	public int dmg = 50;
	
	void OnTriggerEnter2D(Collider2D col){
		if(col.isTrigger == false && col.CompareTag("Ennemi")){
			col.SendMessageUpwards("Dommage",dmg);
		}
	}
}
