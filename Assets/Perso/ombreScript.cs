﻿using UnityEngine;
using System.Collections;

public class ombreScript : MonoBehaviour {

	private Collider2D coll;

	void OnTriggerEnter2D(Collider2D col){

		if(col.isTrigger == true && col.CompareTag("DetectPerso")){
			coll = col;
			col.SendMessageUpwards("Hide",false);
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if (coll != null) {
			if (coll.isTrigger == true && coll.CompareTag ("DetectPerso")) {
				coll.SendMessageUpwards ("Hide", true);
			}
		}
	}
}
