﻿using UnityEngine;
using System.Collections;

public class leverDoor : MonoBehaviour {

    //RigibBody que l'on souhaite animé
    public Transform rigid;
    private Animator _anim;
    private Animator _door;
    private bool _up = true;
    public HeroControllerScript hero;

    // Use this for initialization
    void Start()
    {
        _anim = GetComponent<Animator>();
        _door = rigid.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void useLever()
    {
        if (_up == true)
        {
            hero.useObject = false;
            _anim.Play("leverDown");
            _door.Play("DoorUp");
            _up = false;
        }
        else
        {
            hero.useObject = false;
            _anim.Play("leverUp");
            _door.Play("DoorDown");
            _up = true;
        }
    }

}
