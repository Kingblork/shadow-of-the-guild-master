﻿using UnityEngine;
using System.Collections;

public class leverScript : MonoBehaviour {

    //RigibBody que l'on souhaite faire tomber
    public Rigidbody2D rigid;
    private Animator _anim;
    private bool _up = true;
    

    // Use this for initialization
    void Start () {
	    _anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void useLever()
    {
        if(_up == true)
        {
            
            _anim.Play("leverDown");
            _up = false;
            rigid.isKinematic = false;
        }
        else
        {
            _anim.Play("leverUp");
            _up = true;
        }
    }

}
