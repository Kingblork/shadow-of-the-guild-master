﻿using UnityEngine;
using System.Collections;

public class SerrureScript : MonoBehaviour
{

    //RigibBody que l'on souhaite faire tomber
    public Transform rigid;
    private Animator _door;
    // Use this for initialization
    void Start()
    {
        _door = rigid.GetComponent<Animator>();
       

    }
    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("cle"))
        {
            _door.Play("DoorUp");
            //col.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
