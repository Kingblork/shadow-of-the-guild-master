﻿using UnityEngine;
using System.Collections;

public class NextLevelDoorScript : MonoBehaviour {

    public Transform doorNextDoor;
    public LoadNextLevel scriptNextDoor;
    public HeroControllerScript hero;

    // Use this for initialization
    void Start () {
        //scriptNextDoor = doorNextDoor.GetComponent<LoadNextLevel>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Perso")
           
            hero.changeScene = true;
        hero.allowAttack = false;
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Perso")

            hero.changeScene = false;
        hero.allowAttack = true;
    }


}
