﻿using UnityEngine;
using System.Collections;

public class LoadNextLevel : MonoBehaviour
{

    //Next level to load
    public AsyncOperation op = null;
    public Transform rigid;
    public string scene;

    private bool _allowToChange = false;
    private Animator _door;


    // Use this for initialization
    void Start()
    {
        _door = rigid.GetComponent<Animator>();

    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("UseAttack") && _allowToChange == true)
        {
            StartCoroutine(allowScene());
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Perso")
        {
            _allowToChange = false;
        }
        }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Perso")
        {
            if (op == null)
            {
                if (_door != null)
                {
                    _door.Play("DoorDown");
                }

                _allowToChange = true;

                    //op = Application.LoadLevelAsync(scene);
                    //op.allowSceneActivation = false;
                
            }
        }
    }

   IEnumerator allowScene()
    {
        //if (op != null)
        //{
            
            float fadeTime = GameObject.Find("_GM").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
            Application.LoadLevel(scene);

        //}
    }

    public void NextScene()
    {
        if (op == null)
        {
            _door.Play("DoorDown");
            
            //op.allowSceneActivation = false;
        }
    }

    IEnumerator GOGOGO(float temps)
    {
        while (true)
        {
            yield return new WaitForSeconds(temps);
        }
    }


}
