﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{

    //Next level to load
    public AsyncOperation op = null;
    public Transform rigid;
    public int scene;


    // Use this for initialization
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
           
    }

    public void LoadSceneNow()
    {
        StartCoroutine(allowScene());
    }

    IEnumerator allowScene()
    {
        //if (op != null)
        //{

        float fadeTime = GameObject.Find("_GM").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        SceneManager.LoadScene(scene);

        //}
    }

    IEnumerator GOGOGO(float temps)
    {
        while (true)
        {
            yield return new WaitForSeconds(temps);
        }
    }


}
