﻿using UnityEngine;
using System.Collections;

public class ManaBarrel : MonoBehaviour {


    private Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GetManaBarrel(){
        //Appel de la fonction de destruction de l'objet
        anim.Play("ManaBarrelGet");
        GameObject.Destroy(gameObject, 1);
    }
}
