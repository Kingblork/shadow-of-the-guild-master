﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class SpyMaster_lvl0_sc1 : MonoBehaviour
{
    #region Propriété
    public HeroControllerScript hero;

    private int currentDialog = 0;
    private List<string> listOfDialog = new List<string>();
    #endregion

    #region méthode générale
    // Use this for initialization
    void Start()
    {
       
        //on masque le dialogue du personnage
        GameObject.Find("TextPNJ").GetComponent<Text>().enabled = false;
        GameObject.Find("ScrollerPNJ").GetComponent<Image>().enabled = false;
        //On enregistre les dialogues dans la liste
            listOfDialog.Add("La salle d'entrainement t'attend Yoràn. Va faire tes preuves.");
            listOfDialog.Add("Tu es encore là ?");
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("UseAttack"))
        {
            showDialog();
        }

    }
    #endregion

    #region méthode
    public void showDialog()
    {
        GameObject.Find("TextPNJ").GetComponent<Text>().text = listOfDialog[currentDialog];

        GameObject.Find("TextPNJ").GetComponent<Text>().enabled = true;
        GameObject.Find("ScrollerPNJ").GetComponent<Image>().enabled = true;

        if (listOfDialog.Count > currentDialog + 1)
        {
            currentDialog++;
        }

    }
    #endregion

    #region Collision
    // Si le collider entre en collison avec le personnage
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Perso")
        {
            hero = col.GetComponent<HeroControllerScript>();

            if (hero != null)
            {
                hero.allowAttack = false;
                GameObject.Find("Use").GetComponent<SpriteRenderer>().enabled = true;
            }
        }
    }

    // Si le collider n'est plus en contact avec le personnage
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Perso")
        {
            hero = col.GetComponent<HeroControllerScript>();

            if (hero != null)
            {
                hero.allowAttack = true;
                GameObject.Find("Use").GetComponent<SpriteRenderer>().enabled = true;
            }

            GameObject.Find("TextPNJ").GetComponent<Text>().enabled = false;
            GameObject.Find("ScrollerPNJ").GetComponent<Image>().enabled = false;
        }
    }
    #endregion
}
