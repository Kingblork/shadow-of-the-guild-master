﻿using UnityEngine;
using System.Collections;

public class PlayerCoup1 : MonoBehaviour
{
    private Animator _animator;
    private HeroControllerScript _player;

    private double timer;
    public double Recovery;

    void Start()
    {
        _player = gameObject.GetComponentInParent<HeroControllerScript>();
        _animator = _player.GetComponentInParent<Animator>();
    }

    void Update()
    {
        #region Quand bloquer le coup
        //pdt les autres coups
        if (_player.HittingCoup2 || _player.HittingCoup3 || _player.HittingCoup4 || _player.HittingCoup5) return;
        #endregion

        // maj timer qd le coup est pas lancé
        if (!_player.HittingCoup1) timer = 0;

        // Quand valider l'input : pressage de X, qd aucun coup n'est lancé
        if (Input.GetButtonDown("UseAttack") && !_player.Coup1 && !_player.Coup2 && !_player.Coup3 && !_player.Coup4 && !_player.Coup5 && _player.allowAttack == true)
        {
            _player.Coup1 = true;
        }

        //Quand le coup est demandé, pas lancé et que le coup d'avant est fini
        if (_player.Coup1 && !_player.HittingCoup1)
        {
            _player.freezeFight = true;

            // maj animator en fonction de la serie de coup
            _animator.SetBool("X", true);
            _animator.SetBool("XX", false);
            _animator.SetBool("XXX", false);
            _animator.SetBool("XXXX", false);
            _animator.SetBool("XXXXX", false);
            _player.HittingCoup1 = true;
            _player.Coup1 = false;
        }

        // Quand on a fini d'executer le coup
        if (_player.HittingCoup1 && timer > Recovery)
        {
            _player.freezeFight = false;
            _player.HittingCoup1 = false;
            _animator.SetBool("X", false);
        }

        timer++;
    }

    #region Collisons
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            //Debug.Log("OnTriggerStay2D PAF ! ");
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            //Debug.Log("OnTriggerEnter2D PAF ! ");
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            //Debug.Log("OnTriggerExit2D PAF ! ");
        }
    }
    #endregion

}
