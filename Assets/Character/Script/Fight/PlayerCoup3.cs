﻿using UnityEngine;
using System.Collections;

public class PlayerCoup3 : MonoBehaviour
{
    private Animator _animator;
    private HeroControllerScript _player;

    private double timer;
    public double Recovery;

    void Start()
    {
        _player = gameObject.GetComponentInParent<HeroControllerScript>();
        _animator = _player.GetComponentInParent<Animator>();
    }

    void Update()
    {
        #region Quand bloquer le coup
        //pdt les autres coups sauf celui d'avant
        if (_player.HittingCoup1 || _player.HittingCoup4 || _player.HittingCoup5) return;

        // si player en l'air
        if (!_player.hitGround) return;
        #endregion

        // maj timer qd le coup est pas lancé
        if (!_player.HittingCoup3) timer = 0;

        // Quand valider l'input : pressage de X, qd le coup est pas lancé, qd le coup d'avant est en train d'etre executer
        if (Input.GetButtonDown("UseAttack") && !_player.Coup3 && _player.HittingCoup2 && _player.allowAttack == true)
        {
            _player.Coup3 = true;
        }

        //Quand le coup est demandé, pas lancé et que le coup d'avant est fini
        if (_player.Coup3 && !_player.HittingCoup3 && !_player.HittingCoup2)
        {
            _player.freezeFight = true;

            // maj animator en fonction de la serie de coup
            _animator.SetBool("X", false);
            _animator.SetBool("XX", false);
            _animator.SetBool("XXX", true);
            _animator.SetBool("XXXX", false);
            _animator.SetBool("XXXXX", false);
            _player.HittingCoup3 = true;
            _player.Coup3 = false;
        }

        // Quand on a fini d'executer le coup
        if (_player.HittingCoup3 && timer > Recovery)
        {
            _player.freezeFight = false;
            _player.HittingCoup3 = false;
            _animator.SetBool("XXX", false);
        }

        timer++;
    }

    #region Collisons
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            //Debug.Log("OnTriggerStay2D PAF ! ");
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            //Debug.Log("OnTriggerEnter2D PAF ! ");
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            //Debug.Log("OnTriggerExit2D PAF ! ");
        }
    }
    #endregion

}
