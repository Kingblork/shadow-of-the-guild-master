﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour
{
    public int dmg = 50;

    public Transform explosion;
    public Transform myExplosion;
    // Use this for initialization
    void Start()
    {
        Destroy(gameObject, 5);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.isTrigger == false && col.CompareTag("Ennemi"))
        {
            col.SendMessageUpwards("Dommage", dmg);
            myExplosion = Instantiate(explosion) as Transform;
            myExplosion.position = transform.position;
            Destroy(gameObject,0.1f);
        }
    }

}
