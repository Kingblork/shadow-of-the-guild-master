﻿using UnityEngine;
using System.Collections;

public class ChainCombo : MonoBehaviour
{
    private Animator _animator;
    private Combo _combo1;
    private long _frameTimer;

    private string _debugText;

    public class Coup
    {
        public KeyCode touche;
        public string nomAnime;
        public float nbFrame;
        public bool playing;

        public Coup(KeyCode k, string s, float f)
        {
            touche = k;
            nomAnime = s;
            nbFrame = f;
            playing = false;
        }

    }

    public class Combo
    {
        private int _nbCoup;
        private int _idCoupEnCours;
        private bool _enCombo;

        public Combo precedent;
        public Coup[] enchainement;


        public Combo(int i)
        {
            _nbCoup = i;
            _enCombo = false;
            _idCoupEnCours = 0;
            enchainement = new Coup[i];
            precedent = null;
        }

        public void CheckCombo(Animator anime, ref long frameTimer, ref string debug)
        {
            debug = "id : " + _idCoupEnCours.ToString();

            if (!_enCombo)
            {

                if (Input.GetKeyDown(enchainement[0].touche))
                {
                    debug += "coup 1";

                    frameTimer = 0;
                    anime.Play(enchainement[0].nomAnime);
                    anime.SetBool("endcombo", false);
                    enchainement[0].playing = true;
                    _enCombo = true;
                    _idCoupEnCours++;
                }
            }
            else
            {
                if (_idCoupEnCours >= _nbCoup)
                {
                    if (frameTimer > enchainement[_idCoupEnCours - 1].nbFrame)
                    {
                        EndAllBoolCombo(anime);
                        anime.SetBool("endcombo", true);
                        _enCombo = false;
                        _idCoupEnCours = 0;
                    }
                    return;
                }

                if (frameTimer > enchainement[_idCoupEnCours - 1].nbFrame)
                {

                    EndAllBoolCombo(anime);
                    anime.SetBool("endcombo", true);
                    _enCombo = false;
                    _idCoupEnCours = 0;
                }
                else
                {
                    if (Input.GetKeyDown(enchainement[_idCoupEnCours].touche))
                    {
                        frameTimer = 0;
                        EndAllBoolCombo(anime);
                        anime.SetBool(enchainement[_idCoupEnCours].nomAnime, true);
                        anime.SetBool("endcombo", false);
                        enchainement[_idCoupEnCours].playing = true;
                        _enCombo = true;
                        _idCoupEnCours++;
                    }
                }

            }


        }

        private void EndAllBoolCombo(Animator anime)
        {
            for (int i = 0; i < _nbCoup; i++)
            {
                anime.SetBool(enchainement[i].nomAnime, false);
                
            }
        }
    }


    // Use this for initialization
    void Start()
    {
        _frameTimer = 0;
        _animator = gameObject.GetComponent<Animator>();

        _combo1 = new Combo(6);
        _combo1.enchainement[0] = new Coup(KeyCode.F, "coup1", 50);
        _combo1.enchainement[1] = new Coup(KeyCode.F, "coup2", 50);
        _combo1.enchainement[2] = new Coup(KeyCode.F, "coup3", 50);
        _combo1.enchainement[3] = new Coup(KeyCode.G, "coup4", 50);
        _combo1.enchainement[4] = new Coup(KeyCode.F, "coup5", 50);
        _combo1.enchainement[5] = new Coup(KeyCode.G, "coup6", 50);


    }

    // Update is called once per frame
    void Update()
    {
        //_combo1.CheckCombo(_animator, ref _frameTimer, ref _debugText);
        //_frameTimer++;
    }


    void OnGUI()
    {
        //GUI.Box(new Rect(500, 10, 300, 60), _debugText + "\n" + _combo1.enchainement[0].nbFrame + " = " + _frameTimer);
    }
}
