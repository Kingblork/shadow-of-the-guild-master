﻿using UnityEngine;
using System.Collections;

public class HeroAttack : MonoBehaviour
{

    public bool attacking = false;
    //private float attackTimer = 0;
    //private float attackCD = 1f;
    //private float comboDelay = 0.3f;
    public float attackDelay=0.5f;
    //private bool hit2 = true;
    //private bool hit3 = false;
    //private bool hit5 = false;
    public bool move = false;
    public bool endCombo = false;
    public HeroControllerScript hero;
    public Collider2D triggerAttack;
    public Collider2D sphereAttack;
    public Collider2D BoltTrigger;

    private Animator anim;
    private Behaviour halo;


    void Awake()
    {
        //Récupération du composant d'animation (pour pouvoir jouer les animations de mon choix)
        anim = gameObject.GetComponent<Animator>();
        //Les triggers d'attaque sont désactivés
        //triggerAttack.enabled = false;
        //sphereAttack.enabled = false;
        //BoltTrigger.enabled = false;
        //halo = (Behaviour)GetComponent("Halo");
        //halo.enabled = false;
    }

    void Update()
    {
        
        //Si on presse ctrl gauche et qu'on est pas déjà entrain d'attaquer
        if (Input.GetKeyDown("left ctrl") && hero.hitGround == true)
        {
            if (!attacking)
            {
                //On passe la variable d'attaque à true
                endCombo = false;
                attacking = true;
                anim.SetBool("endcombo", false);
                anim.Play("newHit");
            }
            else
            {
                if (Input.GetKeyDown("left ctrl") && attackDelay>0)
                {
                    endCombo = false;
                    attackDelay = 0.5f;
                    anim.SetBool("combo",true);
                    anim.SetBool("endcombo", false);
                }
            }
        }

        if (attackDelay <= 0)
        {
            attackDelay = 0.5f;
            anim.SetBool("combo", false);
            anim.SetBool("endcombo", true);
            attacking = false;
            move = false;
        }
        else
        {
            if (attacking == true)
            {
                attackDelay -= Time.deltaTime;
            }
        }

        if(endCombo == true)
        {
            anim.SetBool("endcombo", true);

        }



    }
}
