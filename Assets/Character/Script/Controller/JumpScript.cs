﻿using UnityEngine;
using System.Collections;

public class JumpScript : MonoBehaviour
{

    #region
    //Instance du Script controller du héros
    public HeroControllerScript hero;
    private Animator _anim;
    private Rigidbody2D _rigid;
    #endregion

    void Awake()
    {
        _anim = hero.GetComponent<Animator>();
        _rigid = hero.GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Si on touche le sol Ou qu'on touche un mur grimpable et qu'on appui sur le bouton saut et qu'on est pas entrain d'attaquer
        if ((hero.hitGround == true || hero.touchWall == true) && Input.GetButtonDown("Jump"))
        {
            hero.down = false;
            hero.setRayonSol(0.1f);
            if (hero.grimpe == false)
            {
                hero.jump = true;
                //Si on ne touche pas de mur, on fait un saut normal
                if (hero.touchWall == false)
                {
                    //On effectue le saut
                    _rigid.AddForce(new Vector2(1, 1700));
                    //On joue l'animation de saut
                    _anim.Play("Jump");
                    _anim.SetBool("Jump", true);
                }
                else
                {
                    hero.touchWall = false;
                    hero.fall = false;
                    _rigid.isKinematic = false;
                    //Sinon petit saut
                    _rigid.gravityScale = 1;
                    //On effectue le saut
                    _rigid.AddForce(new Vector2(hero.x * 1000, 1000));
                    //On joue l'animation de saut
                    _anim.Play("Jump");
                    _anim.SetBool("Jump", true);

                }
            }
        }
        else
        {
            //Sinon la variable de saut est à false dans l'animator
            _anim.SetBool("Jump", false);
            hero.jump = false;
        }
    }
}
