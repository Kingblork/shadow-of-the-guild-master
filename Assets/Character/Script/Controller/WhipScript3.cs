﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class WhipScript3 : MonoBehaviour
{

    #region propriété
    //Variable pour fouet
    public Material whipMaterial;
    private List<float> _positions;
    private LineRenderer _lineRenderer;

    //Instance du Script controller du héros
    public HeroControllerScript hero;
    private Animator _anim;
    #endregion

    #region Initialisation
    void Awake()
    {
        _anim = hero.GetComponent<Animator>();
    }

    // Use this for initialization
    void Start()
    {
        hero = GetComponentInParent<HeroControllerScript>();
    }
    #endregion


    #region Mise à jour
    // Update is called once per frame
    void Update()
    {
     
    }
    #endregion

    #region Détection
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.isTrigger == true && col.CompareTag("WhipCatch"))
        {
            if (hero.onWhip == false) { 
            Whipper(true, col.transform);
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.isTrigger == true && col.CompareTag("WhipCatch"))
        {
            GameObject.Find("Whip").GetComponent<SpriteRenderer>().enabled = false;
        }
    }
    #endregion

    #region Méthodes

    /// <summary>
    /// Méthode d'utilisation du fouet
    /// </summary>
    /// <param name="enab">On transmet la valeur booléenne permettant l'utilisation du fouet</param>
    /// <param name="whipable">Element qu'on attrape avec le fouet</param>
    public void Whipper(bool enab, Transform whipable)
    {
        //On dit si on peut utiliser le fouet
        hero.whipEnable3 = enab;
        //on fait apparaitre l'icône ou non
        //GameObject.Find("Whip").GetComponent<SpriteRenderer>().enabled = enab;
        //on ajoute le transform de l'élément a saisir à celui servant de référence a la position de la corde
        hero.whipThing = whipable;
    }

    #endregion
}
