﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class WhipScript : MonoBehaviour
{

    #region propriété
    //Variable pour fouet
    public Material whipMaterial;
    private List<float> _positions;
    public LineRenderer _lineRenderer;

    //Instance du Script controller du héros
    public HeroControllerScript hero;
    private Animator _anim;
    #endregion

    #region Initialisation
    void Awake()
    {
        _anim = hero.GetComponent<Animator>();
        //Masque l'élément Whip
        try
        {
            GameObject.Find("Whip").GetComponent<SpriteRenderer>().enabled = false;

        }
        catch (Exception ex)
        {
            int i = 0;
        }

        //On prépare le Line renderer pour la suite
        try
        {
            hero.whipThing = GameObject.Find("PrefabWhipCatch").GetComponent<Transform>();
        }catch(Exception ex)
        {
            hero.whipThing = null;
        }
        
        //_lineRenderer = gameObject.AddComponent<LineRenderer>();
        _lineRenderer.SetWidth(0.05f, 0.05f);

        _lineRenderer.material = whipMaterial;
    }

    // Use this for initialization
    void Start()
    {
        hero = GetComponentInParent<HeroControllerScript>();
    }
    #endregion


    #region Mise à jour
    // Update is called once per frame
    void Update()
    {
        //Si on peut utiliser le fouet et qu'on appui sur la touche, on joue l'animation du fouet
        if (Input.GetButtonDown("Whip") && (hero.whipEnable == true || hero.whipEnable2 == true || hero.whipEnable3 == true) && hero.onWhip == false)
        {
            GameObject.Find("Whip").GetComponent<SpriteRenderer>().enabled = false;
            _anim.Play("Whip");
            hero.onWhip = true;
            hero.rigid.velocity = Vector3.zero;
            Vector3 theVector = new Vector3(transform.position.x, transform.position.y + 0.8f, 1);
            RecalcularZigZag(theVector, hero.whipThing.position, _lineRenderer);

        }

        //Si on est entrain d'utiliser le fouet et que le timer est supérieur à 0 on déplace le personnage grace au fouet
        if (hero.onWhip == true && hero.whipTimer > 0)
        {
            Vector3 theVector = new Vector3(transform.position.x, transform.position.y + 0.8f, 1);
            //_lineRenderer.SetPosition(0, theVector);
            //_lineRenderer.SetPosition(1, hero.whipThing.position);

            //Vector3[] points = new Vector3[100];
            //points[0] = theVector;
            //points[99] = hero.whipThing.position;
            //for (int i = 1; i < 99; i++)
            //{
            //    points[i] = Vector3.Lerp(theVector, hero.whipThing.position, (float)i/10);

            //}


            if (hero.whipTimer < 10)
            {
                RecalcularZigZag(theVector, hero.whipThing.position,_lineRenderer);
            }


            //on décrémente whipTimer
            hero.whipTimer -= 1f;
            int xWhip = 1;
            if (hero.x < 0)
            {
                xWhip = -1;
            }

            //Translate du perso Si on ne touche pas de mur
            if (hero.touchWall == false)
            {
                if (hero.whipEnable == true)
                {
                    hero.rigid.AddForce(new Vector2(0, 205));
                    //hero.transform.Translate(0, 0.3f, 0);
                }
                else
                {
                    if (hero.whipEnable2 == true)
                    {
                        hero.rigid.AddForce(new Vector2(36*xWhip, 205));
                        //hero.transform.Translate(0.3f, 0.3f, 0);
                    }
                    else
                    {
                        if (hero.whipEnable3 == true)
                        {
                            hero.rigid.AddForce(new Vector2(38 * xWhip, 0));
                            //hero.transform.Translate(0.3f, 0, 0);
                        }
                    }
                }
                //On remet les valeur du fouet à leurs valeurs initiales
                if (hero.whipTimer <= 0)
                {
                    zeroWhip();
                    hero.onWhip = false;
                    hero.whipTimer = 10;
                    hero.whipEnable = false;
                    hero.whipEnable2 = false;
                    hero.whipEnable3 = false;
                }
            }
        }
    }
    #endregion

    #region Détection
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.isTrigger == true && col.CompareTag("WhipCatch"))
        {
            if (hero.onWhip == false)
            {
                Whipper(true, col.transform);
            }

        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.isTrigger == true && col.CompareTag("WhipCatch"))
        {
            GameObject.Find("Whip").GetComponent<SpriteRenderer>().enabled = false;
        }
    }
    #endregion

    #region Méthodes

    /// <summary>
    /// Méthode d'utilisation du fouet
    /// </summary>
    /// <param name="enab">On transmet la valeur booléenne permettant l'utilisation du fouet</param>
    /// <param name="whipable">Element qu'on attrape avec le fouet</param>
    public void Whipper(bool enab, Transform whipable)
    {
        //On dit si on peut utiliser le fouet
        hero.whipEnable = enab;
        //on fait apparaitre l'icône ou non
        GameObject.Find("Whip").GetComponent<SpriteRenderer>().enabled = enab;
        //on ajoute le transform de l'élément a saisir à celui servant de référence a la position de la corde
        hero.whipThing = whipable;
    }

    /// <summary>
    /// Fonction de calcul de l'éclair
    /// </summary>
    /// <param name="theVector">Vecteur d'origine</param>
    /// <param name="heroPosition">Position du héro</param>
    /// <param name="theWhip">le line renderer du fouet</param>
    private void RecalcularZigZag(Vector3 theVector, Vector3 heroPosition, LineRenderer theWhip)
    {
        Vector3 origem = theVector;
        Vector3 destino = heroPosition;
        Vector3[] posicoes = new Vector3[20];
        

        float rMin = -0.8f;
        float rMax = 0.8f;

        posicoes[0] = origem;
        for (int p = 1; p < 20; p++)
        {
            Vector3 pos = Vector3.Lerp(origem, destino, p / 4f);
            pos.x += UnityEngine.Random.Range(rMin, rMax);
            pos.y += UnityEngine.Random.Range(rMin, rMax);
            posicoes[p] = pos;
        }
        posicoes[10 - 1] = destino;

        for (int p = 0; p < 20; p++)
        {
            theWhip.SetPosition(p, posicoes[p]);
        }
    }

    /// <summary>
    /// Le line renderer est remis à 0 sur toutes les positions
    /// </summary>
    private void zeroWhip()
    {
        for (int p = 0; p < 20; p++)
        {
            _lineRenderer.SetPosition(p, new Vector3(0,0,0));
        }
    }

    #endregion
}
