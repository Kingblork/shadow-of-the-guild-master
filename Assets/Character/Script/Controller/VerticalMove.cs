﻿using UnityEngine;
using System.Collections;

public class VerticalMove : MonoBehaviour {

    #region propriétés
    //Instance du Script controller du héros
    public HeroControllerScript hero;
    private Animator _anim;
    private Rigidbody2D _rigid;
    #endregion

    void Awake()
    {
        _anim = hero.GetComponent<Animator>();
        _rigid = hero.GetComponent<Rigidbody2D>();
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (hero.freeze == false)
        {
            //On vérifie si la touche bas et haut ne sont pas actionnées
            /*
            if (hero.y == 0)
            {
                _anim.SetFloat("Down", 0);
                _anim.SetFloat("Up", 0);
            }*/

            //Si on va vers le haut et qu'on est sur un mur
            if (hero.y > 0 && hero.touchWall == true && hero.grimpe == false)
            {
                transform.Translate(0, hero.y * Time.deltaTime * hero.speed, 0);
                _anim.SetFloat("Up", hero.speed);
                _anim.SetFloat("Down", 0);
            }
            //Si on fait vers le haut en étant accroché à un rebord 
            if (hero.y>0.8 && hero.grimpe == true)
            {
                hero.lacheRebord = true;
                hero.grimpeDelay = 2f;
                hero.grimpe = false;
                _rigid.isKinematic = false;
                _anim.Play("HangUp");
                _rigid.AddForce(new Vector2(0, 2000));
                hero.rayonSol = 0.1f;

            }

            if (hero.y == 0 && hero.touchWall == true)
            {
                _anim.SetFloat("Up", 0);
                _anim.SetFloat("Down", 0);
                _anim.SetBool("touchWall", true);
                _anim.Play("HangWall");
            }

            if (hero.lacheRebord == true && hero.grimpeDelay >0)
            {
                hero.grimpeDelay -= 0.1f;
            }


            //Si on va vers le bas et qu'on est sur un mur
            if (hero.y <-0.1f && hero.touchWall == true && hero.grimpe == false)
            {
                transform.Translate(0, hero.y * Time.deltaTime * hero.speed, 0);
                _anim.SetFloat("Down", Mathf.Abs(hero.y) * hero.speed);
                _anim.SetFloat("Up", 0);
            }
            //Si on fait vers le bas en étant accroché à un rebord on lâche le rebord
            if (hero.y <-0.5f && hero.grimpe == true)
            {
                hero.lacheRebord = true;
                hero.grimpeDelay = 0.5f;
                hero.grimpe = false;
                _rigid.isKinematic = false;
                _anim.Play("Fall");
            }

            //On se baisse si on touche le sol et qu'on appui sur la flèche du bas
            if (hero.hitGround == true && hero.y <-0.3f && hero.jump == false && hero.down == false)
            {
                _anim.Play("Down");
                hero.down = true;
                hero.speed = hero.speedDown;
            }

            //On se relève si on touche le sol et qu'on relâche la flèche du bas
            //if (hero.hitGround == true && hero.y < -0.2 && hero.plafond == false)
            //{
            //    //_anim.Play("Rise");
            //    hero.down = false;
            //    hero.speed = hero.speedDown;
            //}

            //On se relève si on touche le sol et qu'on relâche la flèche du bas
            if (hero.hitGround == true && hero.y > 0.2 && hero.plafond == false && hero.down == true)
            {
                _anim.Play("Rise");
                hero.down = false;
                hero.speed = hero.speedDown;
            }

            //if ((hero.y==0 || hero.y > 0.2) && hero.down == true && hero.plafond == false)
            //{
            //    _anim.Play("Rise");
            //    hero.down = false;
            //    hero.speed = hero.speedRun;
            //}

        }
    }
}
