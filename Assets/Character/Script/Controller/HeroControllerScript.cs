﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HeroControllerScript : MonoBehaviour
{
    #region Propriétés
    //Variable générale
    public int mana = 100;
    public float y = 0;
    public float x = 0;
    public float speed = 10f;
    public float health = 1000;
    public float direction = 1;
    public bool hitGround = true;
    public bool hitbyMVilain = false;
    public bool down = false;
    public bool fall = false;
    public bool jump = false;
    public bool touchWall = false;
    public float speedRun = 10f;
    private float _speedJump = 5f;
    public float speedDown = 5f;

    //Variable de freeze des actions du personnage
    public bool freeze = false;
    public bool freezeFight = false;

    //Utiliser un object
    public bool useObject = false;
    private Collider2D _currentObject;

    //Autoriser l'attaque
    public bool allowAttack = false;

    //Variable pour le sol
    public float rayonSol = 0.2f;
    public LayerMask Sol;
    public Transform checkSol;

    //Grimper au rebord
    public float grimpeDelay = 0f;
    public bool grimpe = false;
    public bool touchRebord = false;
    public bool lacheRebord = false;

    //Whip
    public float whipTimer = 10;
    public bool whipEnable { get; set; }
    public bool whipEnable2 { get; set; }
    public bool whipEnable3 { get; set; }
    public bool onWhip { get; set; }
    public Transform whipThing { get; set; }

    //Se baisser et avancer baisser
    public bool plafond = false;

    //bolt spell
    public Transform BoltBall;
    public Transform myBoltBall = null;
    public float delayBoltBall = 100;
    public bool shootBoltBall;

    //Script et Transform du personnage
    public HeroControllerScript heroController;
    //public HeroAttack hero;
    private Transform transformCache;

    //Camera
    public CameraController camera;
    private CameraController cam;

    //Gestion du changement de scene
    public bool changeScene = false;

    //Gestion PNJ
    public bool dialogPNJ = false;


    public Animator anim;
    public Rigidbody2D rigid;
    public PlayerLife life;
    public PlayerMana manaBar;
    #endregion

    #region Parametres combo
    public bool Coup1;
    public bool Coup2;
    public bool Coup3;
    public bool Coup4;
    public bool Coup5;
    public bool HittingCoup1;
    public bool HittingCoup2;
    public bool HittingCoup3;
    public bool HittingCoup4;
    public bool HittingCoup5;
    #endregion

    // Use this for initialization
    void Start()
    {
        cam = Camera.main.GetComponent<CameraController>();
        //Initialisation des variable
        anim = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
        life = GetComponent<PlayerLife>();
        life.maxHealth = (int)health;
        manaBar = GetComponent<PlayerMana>();
        manaBar.maxMana = mana;
        whipEnable = false;
        onWhip = false;

        #region Initialisation parametres combo
        HittingCoup1 = false;
        HittingCoup2 = false;
        HittingCoup3 = false;
        Coup1 = false;
        Coup2 = false;
        Coup3 = false;
        #endregion
    }

    void Awake()
    {
        //Transformcache reçoit le transforme du personnage
        transformCache = transform;

        try
        {
            GameObject.Find("Use").GetComponent<SpriteRenderer>().enabled = false;
        }
        catch (Exception ex)
        {
            int i = 0;
        }
    }

    void FixedUpdate()
    {
        if (freeze == false)
        {
            //Test si on touche le sol
            hitGround = Physics2D.OverlapCircle(checkSol.position, rayonSol, Sol);
            anim.SetBool("Ground", hitGround);
            //On empêche le perso de pouvoir partir en sucette
            rigid.MoveRotation(0);
            //On récupère l'axe de déplacement horizontal
            x = Input.GetAxis("Horizontalj");
            //On récupère l'axe de déplacement vertical
            y = Input.GetAxis("Vertical");

        }
    }



    /// <summary>
    /// UPDATE 
    /// </summary>
    // Update is called once per frame
    void Update()
    {
        if (freeze == false)
        {
            //Si on ne touche pas le sol la vitesse de déplacement est divisé par deux
            if (hitGround == true)
            {
                rigid.mass = 5;
                rigid.gravityScale = 1;
                touchWall = false;
                speed = speedRun;
                fall = false;
                rayonSol = 0.2f;
                jump = false;
            }
            else
            {
                speed = _speedJump;
            }

            //Si on ne touche pas de mur, la gravité est normale
            if (touchWall == false)
            {
                rigid.gravityScale = 1;
            }


            //Monter au rebord
            if (grimpe == true)
            {
                rigid.isKinematic = true;
            }
            else
            {
                grimpe = false;
            }

            //Si on tombe on ajoute une force vers le bas
            if (fall == true)
            {
                //rigid.AddForce(new Vector2(1, -1000));
            }

            if(changeScene == true)
            {
                GameObject.Find("Use").GetComponent<SpriteRenderer>().enabled = true;
            }
            else
            {
                GameObject.Find("Use").GetComponent<SpriteRenderer>().enabled = false;
            }
        }
    }

    #region Méthodes
    /// <summary>
    /// Méthode réduisant la vie du personnage
    /// </summary>
    /// <param name="dmg">Valeur du dommage infligé</param>
    public void Dommage(int dmg)
    {
        health -= dmg;
        anim.Play("getCoup");
        life.TakeDamage(dmg);
        hitbyMVilain = true;
    }


    /// <summary>
    /// Méthode déterminant si le personnage peut utiliser le fouet
    /// </summary>
    /// <param name="enab"></param>
    public void NoWhipper(bool enab)
    {
        //On dit si on peut utiliser le fouet
        whipEnable = enab;
        //on fait apparaitre l'icône ou non
        GameObject.Find("Whip").GetComponent<SpriteRenderer>().enabled = enab;
    }

    /// <summary>
    /// Méthode pour l'escalade
    /// </summary>
    public void Climb()
    {
        //On vérifie qu'il n'est pas entrain de monter à un rebord
        if (grimpe == false && jump == false)
        {
            //Il touche un mur grimpable
            touchWall = true;
            //Propriétés physique du perso désactivées
            rigid.isKinematic = true;
            anim.SetBool("touchWall", true);
            anim.Play("HangWall");
        }
    }

    /// <summary>
    /// Méthode quand le personnage ne touche plus un mur grimpable
    /// </summary>
    public void NoClimb()
    {
        //Propritétés physique classique
        rigid.isKinematic = false;
        //Valeur boolèenne de l'animator à false
        anim.SetBool("touchWall", false);
        if(jump == false) {
            touchWall = false;
            anim.Play("Fall");
        }
        down = false;
    }

    /// <summary>
    /// Méthode déclenchant le fait que le personnage s'accroche au mur
    /// </summary>
    public void Hang()
    {
        touchWall = false;
        grimpe = true;
        anim.Play("Hang");
        rigid.velocity = Vector3.zero;
    }

    /// <summary>
    /// Méthode de déplacement par translation du personnage
    /// </summary>
    public void move()
    {
        transformCache.Translate(0.1f, 0, 0);
    }

    /// <summary>
    /// Méthode disant si le personnage touche un plafond
    /// </summary>
    /// <param name="val">True ou False</param>
    public void Down(bool val)
    {
        plafond = val;
    }

    /// <summary>
    /// Méthode indiquant si on est entrain de tomber ou non
    /// </summary>
    /// <param name="val"></param>
    public void fallen(bool val)
    {
        fall = val;
    }

    /// <summary>
    /// Méthode pour lancer une boule d'électricité
    /// </summary>
    public void fireBoltBall()
    {
        //On instantie une BoltBall
        myBoltBall = Instantiate(BoltBall) as Transform;
        //On lui donne la position du personnage
        myBoltBall.position = transformCache.position;

        myBoltBall.GetComponent<MoveScript>().direction = new Vector2(direction * 2, 0);
        shootBoltBall = true;
        mana -= 20;
        manaBar.TakeMana(20);
    }

    /// <summary>
    /// Méthode permettant de récupérer du mana
    /// </summary>
    /// <param name="getMana">Valeur du mana recupéré</param>
    public void GetManaBarrel(int getMana)
    {
        this.mana += getMana;
    }

    /// <summary>
    /// Vitesse devient la vitesse de course
    /// </summary>
    public void setSpeedRun()
    {
        speed = speedRun;
    }

    /// <summary>
    /// Vitesse devient la vitesse de saut
    /// </summary>
    public void setSpeedJump()
    {
        speed = _speedJump;
    }


    /// <summary>
    /// Vitesse devient la vitesse accroupi
    /// </summary>
    public void setSpeedDown()
    {
        speed = speedDown;
    }

    /// <summary>
    /// modifier la valeur du rayon détectant le sol
    /// </summary>
    /// <param name="value">Valeur du rayon (float)</param>
    public void setRayonSol(float value)
    {
        rayonSol = value;
    }

    /// <summary>
    /// Augmente la vitesse de la valeur passée paramètre
    /// </summary>
    /// <param name="value"></param>
    public void increaseSpeed(float value)
    {
        speed += value;
    }
    #endregion

    #region détection
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.isTrigger == true && col.CompareTag("ManaBarrel"))
        {
            GetManaBarrel(200);
            col.SendMessageUpwards("GetManaBarrel");
        }

        if (col.isTrigger == true && col.CompareTag("Lever"))
        {
            useObject = true;
            _currentObject = col;
            GameObject.Find("Use").GetComponent<SpriteRenderer>().enabled = true;
        }

        if (col.isTrigger == true && col.CompareTag("EndOfScene"))
        {
            if (camera != null)
            {
                camera.stay = true;
            }
        }


    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Lever")
        {
            useObject = false;
            _currentObject = null;
            GameObject.Find("Use").GetComponent<SpriteRenderer>().enabled = false;
        }
    }
    #endregion
}
