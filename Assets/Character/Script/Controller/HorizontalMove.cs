﻿using UnityEngine;
using System.Collections;

public class HorizontalMove : MonoBehaviour
{
    #region propriétés
    //Instance du Script controller du héros
    public HeroControllerScript hero;
    private Animator _anim;
    private Rigidbody2D _rigid;
    #endregion

    void Awake()
    {
        _anim = hero.GetComponent<Animator>();
        _rigid = hero.GetComponent<Rigidbody2D>();
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (hero.freeze == false)
        {
            //On vérifie si un des touche de déplacement est actionnée
            if (Input.GetAxis("Horizontalj") > -0.8 && Input.GetAxis("Horizontalj") < 0.8)
            {
                if (hero.hitGround == true)
                {
                    hero.setSpeedRun();
                }
                else
                {
                    hero.setSpeedJump();
                }

                hero.x = 0;
                _anim.SetFloat("Speed", 0);
            }
            //Si c'est vers la droite
            if (hero.x > 0 && hero.touchWall == false && hero.grimpe == false)
            {
                //Incrémentation de la vitesse
                hero.increaseSpeed(0.1f);
                hero.direction = transform.eulerAngles.x;
                //On effectue la translation du personnage
                transform.Translate(hero.x * Time.deltaTime * hero.speed, 0, 0);
                //On le retourne en fonction de la direction
                transform.eulerAngles = new Vector2(0, 0);
                //On incrémente la variable de vitesse dans l'animator
                _anim.SetFloat("Speed", Mathf.Abs(hero.x) * hero.speed);

            }
            //Si c'est vers la gauche
            if (hero.x < 0 && hero.touchWall == false && hero.grimpe == false)
            {
                //Incrémentation de la vitesse
                hero.speed += 0.1f;
                hero.direction = hero.x;
                //On effectue la translation du personnage
                transform.Translate(-hero.x * Time.deltaTime * hero.speed, 0, 0);
                //On le retourne en fonction de la direction
                transform.eulerAngles = new Vector2(0, 180);
                //On incrémente la variable de vitesse dans l'animator
                _anim.SetFloat("Speed", Mathf.Abs(hero.x) * hero.speed);
            }
        }

    }
}
