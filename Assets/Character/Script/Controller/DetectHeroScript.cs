﻿using UnityEngine;
using System.Collections;

public class DetectHeroScript : MonoBehaviour {
    public Collider2D coll;
    public Collider2D baisser;

    void Awake()
    {


        coll = GetComponent<Collider2D>();
    }

    public void Hide(bool valeur)
    {
        coll.enabled = valeur;
        baisser.enabled = valeur;
    }

}
