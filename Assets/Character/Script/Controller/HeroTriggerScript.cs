﻿using UnityEngine;
using System.Collections;

public class HeroTriggerScript : MonoBehaviour
{

    public HeroControllerScript hero;
    private CameraController cam;
    // Use this for initialization
    void Start()
    {
        hero = GetComponentInParent<HeroControllerScript>();
        cam = Camera.main.GetComponent<CameraController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.isTrigger == true && col.CompareTag("rebord"))
        {
            if (hero.grimpeDelay <= 0)
            {
                hero.Hang();
            }
        }
        else
        {
            if (col.isTrigger == false && col.CompareTag("MurGrimpable"))
            {
                hero.Climb();
            }
            else
            {
                if (col.isTrigger == false && col.CompareTag("plafond"))
                {
                    hero.Down(true);
                }
                else
                {

                }
            }
        }

        if (col.isTrigger == true && col.CompareTag("followPlayer"))
        {
            cam.alignCharacter();
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.isTrigger == false && col.CompareTag("MurGrimpable"))
        {
            hero.NoClimb();
        }
        else
        {
            if (col.isTrigger == false && col.CompareTag("plafond"))
            {
                hero.Down(false);
            }
        }
    }

}
