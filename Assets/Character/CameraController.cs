﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    public Transform Heros;
    public bool stay { get; set; }
    public HeroControllerScript hero;

    // Use this for initialization
    void Start()
    {
        stay = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (stay == false)
        {
            if (hero.direction == 0)
            {
                if (Heros.position.x - 7 > transform.position.x || Heros.position.y - 5 > transform.position.y)
                {
                    transform.position = new Vector3(Heros.position.x, Heros.position.y + 2, -3);
                }
            }
            else
            {
                if (Heros.position.x + 7 < transform.position.x || Heros.position.y - 5 > transform.position.y)
                {
                    transform.position = new Vector3(Heros.position.x, Heros.position.y + 2, -3);
                }
            }
        }

        if (hero.fall == true && Heros.position.y - 1 < transform.position.y)
        {
            transform.position = new Vector3(Heros.position.x, Heros.position.y + 2, -3);
        }

    }

    /// <summary>
    /// si le trigger du perso touche un élement tag en followPlayer alors on réaligne tout de suite la caméra
    /// </summary>
    public void alignCharacter()
    {

        transform.position = new Vector3(Heros.position.x, Heros.position.y + 2, -3);

    }



}
