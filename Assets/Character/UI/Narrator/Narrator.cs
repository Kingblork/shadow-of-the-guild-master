﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Narrator : MonoBehaviour
{
    #region propriétés
    public float letterPause = 0.05f;
    public AudioClip typeSound1;
    public AudioClip typeSound2;
    private SpriteRenderer _scroller;
    private Image _dark;
    private Image _yoran;
    private bool _onRead = false;
    private List<string> _listText;
    private int _currentText = 0;
    private bool _readAll = false;
    private bool _endingCurrentText = false;
    public bool onRead = false;
    private bool alreadyRead = false;

    private Transform _heroTransform = null;
    private Transform _yoranTransform = null;
    private Transform _scrollerTransform = null;
    public HeroControllerScript hero;
    public JumpScript jumpScript;

    private string _message;
    private Text _textComp;
    #endregion

    // Use this for initialization
    void Start()
    {
        _scroller = GameObject.Find("Scroller").GetComponent<SpriteRenderer>();
        _scrollerTransform = _scroller.transform;
        _heroTransform = hero.transform;
        _dark = GameObject.Find("Dark").GetComponent<Image>();
        _yoran = GameObject.Find("yoran-basic").GetComponent<Image>();
        _yoranTransform = _yoran.transform;
        _scroller.enabled = false;
        _dark.enabled = false;
        _yoran.enabled = false;
        _textComp = GetComponent<Text>();
        _textComp.text = "";
        _textComp.enabled = false;

    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && onRead == true)
        {
            if (_readAll == false && _endingCurrentText == false)
            {
                readAll();
            }
            else
            {
                _endingCurrentText = false;
                nextText();
            }
        }

        _scrollerTransform.position = new Vector3(_heroTransform.position.x + 1.8f, _heroTransform.position.y, -2f);
        //_yoranTransform.position = new Vector3(_heroTransform.position.x -4.2f, _heroTransform.position.y+0.5f, -2f);
    }


    /// <summary>
    /// Ecrire le texte lettre par lettre
    /// </summary>
    /// <returns></returns>
    IEnumerator TypeText()
    {
        if (_readAll == false)
        {
            foreach (char letter in _message.ToCharArray())
            {
                if (_readAll == true)
                {
                    break;
                }
                _textComp.text += letter;
                //if (typeSound1 && typeSound2)
                //SoundManager.instance.RandomizeSfx(typeSound1, typeSound2);
                yield return 0;
                yield return new WaitForSeconds(letterPause);
            }
            _endingCurrentText = true;
        }
    }

    /// <summary>
    /// Commencer l'écriture du texte
    /// </summary>
    public void ReadNarrator()
    {
        StartCoroutine(TypeText());
    }

    /// <summary>
    /// Initialisation du narrateur en passant une liste de string
    /// </summary>
    /// <param name="listText">Liste des textes à afficher</param>
    public void initNarrator(List<string> listText)
    {
        hero.freeze = true;
        hero.enabled = false;
        jumpScript.enabled = false;
        onRead = true;
        _textComp.enabled = true;
        //_scroller.enabled = true;
        _dark.enabled = true;
        _yoran.enabled = true;
        _listText = listText;
        _message = _listText[_currentText];
        ReadNarrator();
    }

    /// <summary>
    /// Lire tout le texte en cours
    /// </summary>
    private void readAll()
    {
        _readAll = true;
        _textComp.text = _message;
    }

    /// <summary>
    /// Passer au message suivant
    /// </summary>
    private void nextText()
    {
        _textComp.text = "";
        _readAll = false;
        _currentText++;
        if (_currentText + 1 > _listText.Count)
        {
            endText();
        }
        else
        {
            _message = _listText[_currentText];
            ReadNarrator();
        }
    }

    /// <summary>
    /// Fin de la narration, masquage des éléments et remise aux valeurs initiales
    /// </summary>
    private void endText()
    {
        onRead = false;
        _textComp.text = "";
        _textComp.enabled = false;
        _scroller.enabled = false;
        _dark.enabled = false;
        _yoran.enabled = false;
        _currentText = 0;
        _readAll = false;
        hero.freeze = false;
        hero.enabled = true;
        jumpScript.enabled = true;
        alreadyRead = true;
        _listText = null;
    }

}