﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Dialog : MonoBehaviour
{
    #region propriétés
    public HeroControllerScript hero;
    public JumpScript jumpScript;
    public bool onRead = false;
    public float letterPause = 0.05f;
    public AudioClip typeSound1;
    public AudioClip typeSound2;
    private SpriteRenderer _scrollerG;
    private SpriteRenderer _scrollerD;
    private Image _dark;
    private bool _onRead = false;
    private List<string> _listText;
    private int _currentText = 0;
    private bool _readAll = false;
    private bool _endingCurrentText = false;

    private bool alreadyRead = false;
    private string[,] _listDialog;

    private Image _currentTransform = null;
    private Transform _heroTransform = null;
    private Transform _scrollerDTransform = null;
    private Transform _scrollerGTransform = null;

    private RectTransform _textD;
    private RectTransform _textG;

    private string _message;
    private Text _textCompD;
    private Text _textCompG;
    private string currentSprite;
    #endregion

    // Use this for initialization
    void Start()
    {
        //GET ELEMENT
        _textD = GameObject.Find("TextDialogD").GetComponent<RectTransform>();
        _textG = GameObject.Find("TextDialogG").GetComponent<RectTransform>();
        //_scrollerG = GameObject.Find("Scroller-G").GetComponent<SpriteRenderer>();
        //_scrollerD = GameObject.Find("Scroller-D").GetComponent<SpriteRenderer>();
        //_scrollerGTransform = _scrollerG.transform;
        //_scrollerDTransform = _scrollerD.transform;
        _heroTransform = hero.transform;
        _dark = GameObject.Find("Dark").GetComponent<Image>();
        _textCompD = GameObject.Find("TextDialogD").GetComponent<Text>();
        _textCompG = GameObject.Find("TextDialogG").GetComponent<Text>();
        //INIT ELEMENT
        GameObject.Find("yoran-basic").GetComponent<Image>().enabled = false;
        //_scrollerG.enabled = false;
        //_scrollerD.enabled = false;
        _dark.enabled = false;
        _textCompD.text = "";
        _textCompD.enabled = false;
        _textCompG.text = "";
        _textCompG.enabled = false;

    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && onRead == true)
        {
            if (_readAll == false && _endingCurrentText == false)
            {
                readAll();
            }
            else
            {
                _endingCurrentText = false;
                nextText();
            }
        }


        //_scrollerGTransform.position = new Vector3(_heroTransform.position.x + 1.8f, _heroTransform.position.y, -1.6f);
        //_scrollerDTransform.position = new Vector3(_heroTransform.position.x - 1.8f, _heroTransform.position.y, -1.6f);

    }


    /// <summary>
    /// Ecrire le texte lettre par lettre
    /// </summary>
    /// <returns></returns>
    IEnumerator TypeText()
    {
        if (_readAll == false)
        {
            foreach (char letter in _message.ToCharArray())
            {
                if (_readAll == true)
                {
                    break;
                }
                if (_listDialog[_currentText, 0] == "D")
                {
                    _textCompD.text += letter;
                }
                else
                {
                    _textCompG.text += letter;
                }
                //if (typeSound1 && typeSound2)
                //SoundManager.instance.RandomizeSfx(typeSound1, typeSound2);
                yield return 0;
                yield return new WaitForSeconds(letterPause);
            }
            _endingCurrentText = true;
        }
    }

    /// <summary>
    /// Commencer l'écriture du texte
    /// </summary>
    public void ReadNarrator()
    {
        StartCoroutine(TypeText());
    }

    /// <summary>
    /// Initialisation du narrateur en passant une liste de string
    /// </summary>
    /// <param name="listText">Liste des textes à afficher</param>
    public void initDialog(string[,] dialog)
    {
        hero.freeze = true;
        hero.enabled = false;
        jumpScript.enabled = false;
        onRead = true;

        _listDialog = dialog;
        //Current Sprite
        currentSprite = _listDialog[_currentText, 1];
        //Donner les transform pour positionnement
        _currentTransform = GameObject.Find(currentSprite).GetComponent<Image>();
        if (_listDialog[_currentText, 0] == "D")
        {
            _textCompD.enabled = true;
            //_scrollerD.enabled = true;
            //_scrollerG.enabled = false;
            _textCompG.enabled = true;

        }
        else
        {
            _textCompG.enabled = true;
            //_scrollerG.enabled = true;
            //_scrollerD.enabled = false;
            _textCompD.enabled = false;
        }

        GameObject.Find(currentSprite).GetComponent<Image>().enabled = true;
        _dark.enabled = true;
        _message = _listDialog[_currentText, 2];
        ReadNarrator();
    }

    /// <summary>
    /// Lire tout le texte en cours
    /// </summary>
    private void readAll()
    {
        _readAll = true;
        if (_listDialog[_currentText, 0] == "D")
        {
            _textCompD.text = _message;
        }
        else
        {
            _textCompG.text = _message;
        }
    }

    /// <summary>
    /// Passer au message suivant
    /// </summary>
    private void nextText()
    {
        if (_listDialog[_currentText, 0] == "D")
        {
            _textCompD.text = "";
        }
        else
        {
            _textCompG.text = "";
        }
        _readAll = false;
        //_currentText++;

        if (_currentText + 1 > _listDialog.Length / 4)
        {
            endText();
        }
        else
        {
            changeDialog();
            _message = _listDialog[_currentText, 2];
            ReadNarrator();
        }
    }

    /// <summary>
    /// Fin de la narration, masquage des éléments et remise aux valeurs initiales
    /// </summary>
    private void endText()
    {
        onRead = false;
        _textCompG.text = "";
        _textCompD.text = "";

        hideAll();
        _currentText = 0;
        _readAll = false;
        hero.freeze = false;
        hero.enabled = true;
        jumpScript.enabled = true;
        alreadyRead = true;
        _listText = null;
    }

    private void hideAll()
    {
        GameObject.Find("yoran-basic").GetComponent<Image>().enabled = false;
        GameObject.Find("secretMaster").GetComponent<Image>().enabled = false;
        _textCompG.enabled = false;
        _textCompD.enabled = false;
        //_scrollerG.enabled = false;
        //_scrollerD.enabled = false;
        _dark.enabled = false;
    }

    private void changeDialog()
    {
        _currentText++;
        GameObject.Find(currentSprite).GetComponent<Image>().enabled = false;
        if (_listDialog[_currentText, 0] == "D")
        {
            //_scrollerD.enabled = true;
            //_scrollerG.enabled = false;
        }
        else
        {
            //_scrollerG.enabled = true;
            //_scrollerD.enabled = false;
        }
        currentSprite = _listDialog[_currentText, 1];
        _message = _listDialog[_currentText, 2];
        GameObject.Find(currentSprite).GetComponent<Image>().enabled = true;
        _currentTransform = GameObject.Find(currentSprite).GetComponent<Image>();
    }

}