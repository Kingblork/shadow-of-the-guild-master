﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Narrator_lvl1_5 : MonoBehaviour
{

    private List<string> listText = new List<string>();
    private bool alreadyRead = false;

    public Narrator narrator;
    // Use this for initialization
    void Start()
    {
        listText.Add("Enjoy... A narrow passage...");
        listText.Add("I have to go down with the left joystick and the forward.");
        listText.Add("John Maclane style !");

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Perso"))
        {
            if (narrator.onRead == false)
            {
                if (alreadyRead == false)
                {
                    alreadyRead = true;
                    narrator.initNarrator(listText);
                }
            }
        }
    }
}
