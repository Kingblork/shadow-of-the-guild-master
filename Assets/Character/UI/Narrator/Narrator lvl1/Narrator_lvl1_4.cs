﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Narrator_lvl1_4 : MonoBehaviour
{

    private List<string> listText = new List<string>();
    private bool alreadyRead = false;

    public Narrator narrator;
    // Use this for initialization
    void Start()
    {
        listText.Add("Oh yeah, my favorite moment.");
        listText.Add("I can use my electric whip to cross big distance.");
        listText.Add("I have to use R1 to use my whip.");

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Perso"))
        {
            if (narrator.onRead == false)
            {
                if (alreadyRead == false)
                {
                    alreadyRead = true;
                    narrator.initNarrator(listText);
                }
            }
        }
    }
}
