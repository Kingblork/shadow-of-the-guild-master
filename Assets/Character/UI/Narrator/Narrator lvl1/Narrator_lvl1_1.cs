﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Narrator_lvl1_1 : MonoBehaviour
{

    private List<string> listText = new List<string>();
    private bool alreadyRead = false;

    public Narrator narrator;
    // Use this for initialization
    void Start()
    {
        listText.Add("Alright, i'm in the training room.");
        listText.Add("To beginning i have to use X to jump and overcome obstacles");

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Perso"))
        {
            if (narrator.onRead == false)
            {
                if (alreadyRead == false) {
                    alreadyRead = true;
                    StartCoroutine(loadScene());
                    narrator.initNarrator(listText);
                }
            }
        }
    }

    IEnumerator loadScene()
    {
        float fadeTime = GameObject.Find("_GM").GetComponent<Fading>().BeginFade(-1);
        yield return new WaitForSeconds(fadeTime);
    }
}
