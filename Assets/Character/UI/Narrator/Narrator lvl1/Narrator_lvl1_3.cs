﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Narrator_lvl1_3 : MonoBehaviour
{

    private List<string> listText = new List<string>();
    private bool alreadyRead = false;

    public Narrator narrator;
    // Use this for initialization
    void Start()
    {
        listText.Add("Good. There is a wood wall.");
        listText.Add("I can climb on wood wall to reach new zones.");
        listText.Add("I just have to jump against the wall and go up or down with the left joystick.");

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Perso"))
        {
            if (narrator.onRead == false)
            {
                if (alreadyRead == false)
                {
                    alreadyRead = true;
                    narrator.initNarrator(listText);
                }
            }
        }
    }
}
