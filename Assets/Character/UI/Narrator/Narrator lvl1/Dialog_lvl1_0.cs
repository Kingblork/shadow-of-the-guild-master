﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dialog_lvl1_0 : MonoBehaviour
{

    private string[,] listDialog;
    private List<string> listText = new List<string>();
    private bool alreadyRead = false;

    public Dialog dialog;
    // Use this for initialization
    void Start()
    {
        listDialog = new string[,]
          {
            {"D","secretMaster", "Welcome Yoràn."},
            {"G","yoran-basic", "Secret Master, i wait for your orders."},
            {"D", "secretMaster", "Your mission in the empire has the hardest task you had to lead. It will be hard. Very Hard..."},
            {"G","yoran-basic", "Shadow of the Guild is not an honorific title. I'm ready for !"},
            {"D", "secretMaster", "Sure... Sure... But you know the protocol. Every mission beginning by a check of your abilities. Go to the training room, and show me you already deserve this title."},
            {"G","yoran-basic", "I will not disappoint you !"},
              
          };

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Perso"))
        {
            if (dialog.onRead == false)
            {
                if (alreadyRead == false)
                {
                    alreadyRead = true;
                    dialog.initDialog(listDialog);
                }
            }
        }
    }
}
