﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Narrator_lvl1_2 : MonoBehaviour {

    private List<string> listText = new List<string>();
    private bool alreadyRead = false;

    public Narrator narrator;
    // Use this for initialization
    void Start()
    {
        listText.Add("Ok, this plateform is too hight.");
        listText.Add("But i can jump and hang the flange. Next i just have to go UP with left Joystick.");

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Perso"))
        {
            if (narrator.onRead == false)
            {
                if (alreadyRead == false)
                {
                    alreadyRead = true;
                    narrator.initNarrator(listText);
                }
            }
        }
    }
}
