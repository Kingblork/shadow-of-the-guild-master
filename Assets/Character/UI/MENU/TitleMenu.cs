﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleMenu : MonoBehaviour {
    private GameObject myCanvasLoad = null;
    private GameObject myCanvasCredit = null;
    private GameObject myCanvasOption = null;
    private GameObject myCanvasGroup = null;
    public Canvas myLoadList = null;
    public UnityEngine.Font myFont = null;
    public Color myColor;
    public GameObject myButton;

    // Use this for initialization
    void Start () {
        myCanvasLoad = GameObject.Find("LoadPanel");
        myCanvasLoad.SetActive(false);
        myCanvasOption = GameObject.Find("OptionPanel");
        myCanvasOption.SetActive(false);
        myCanvasOption = GameObject.Find("CreditPanel");
        myCanvasOption.SetActive(false);
        myColor.a = 255;
        myColor.b = 255;
        myColor.g = 255;
       
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Charger les sauvegarde du jeu
    /// </summary>
   public void LoadSave()
    {
        GameObject button = Instantiate(myButton);
        button.GetComponentInChildren<Text>().font = myFont;
        button.GetComponentInChildren<Text>().color = myColor;
        button.GetComponentInChildren<Text>().text = "Scène 2";
        //button.AddComponent<CanvasRenderer>();
        button.transform.parent = myLoadList.transform;
        button.transform.position = myLoadList.transform.position;

    }
}
