﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{



    public Transform healthTransform;

    private float cachedY;
    private float minXValue;
    private float maxXValue;

    private int currentHealth;
    public int maxHealth;
    public Transform visualHealth;

    public float coolDown;
    private bool onCD;

    private int CurrentHealth
    {
        get
        {
            return currentHealth;
        }

        set
        {
            currentHealth = value;
            HandleHealth();
        }
    }

    // Use this for initialization
    void Start()
    {
        if (healthTransform != null)
        {
            cachedY = healthTransform.position.y;
            maxXValue = healthTransform.position.x;
            // minXValue = healthTransform.position.x - healthTransform.rect.width;
            CurrentHealth = maxHealth;
        }
    }

    IEnumerator CoolDownDmg()
    {
        onCD = true;
        yield return new WaitForSeconds(coolDown);
        onCD = false;
    }


    // Update is called once per frame
    void Update()
    {

    }

    private void HandleHealth()
    {
        float currentXScale = (float)currentHealth / (float)maxHealth;
        //healthTransform.position = new Vector3(currentXvalue, cachedY);
        //healthTransform.localScale = new Vector3(healthTransform.localScale.x * currentXScale, healthTransform.localScale.y);
        /*
                if(CurrentHealth > maxHealth / 2) //plus de 50% de vie
                {
                    visualHealth.color = new Color32((byte)MapValues(CurrentHealth, maxHealth / 2, maxHealth, 255, 0), 255, 0, 255);
                }
                else //moins de 50%
                {
                    visualHealth.color = new Color32(255, (byte)MapValues(CurrentHealth,0, maxHealth / 2, 0, 255), 0, 255);
                }
                */
    }

    private float MapValues(float x, float inMin, float inMax, float outMin, float outMax)
    {
        return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }

    public void TakeDamage(int damage)
    {
        if (damage > CurrentHealth)
        {
            damage = CurrentHealth;
        }


        if (!onCD && currentHealth > 0)
        {
            StartCoroutine(CoolDownDmg());
            CurrentHealth -= damage;
        }

    }
}
