﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMana : MonoBehaviour {



    public Transform manaTransform;

    private float cachedY;
    private float minXValue;
    private float maxXValue;

    private int currentMana;
    public int maxMana;
    public Transform visualMana;

    public float coolDown;
    private bool onCD;

    private int CurrentMana
    {
        get
        {
            return currentMana;
        }

        set
        {
            currentMana = value;
            if(currentMana > maxMana)
            {
                currentMana = maxMana;
            }
            if (currentMana < 0)
            {
                currentMana = 0;
            }

            HandleMana();
        }
    }

    // Use this for initialization
    void Start () {
        if(manaTransform != null) { 
            cachedY = manaTransform.position.y;
            maxXValue = manaTransform.position.x;
            //minXValue = manaTransform.position.x - manaTransform.rect.width;
        }
        CurrentMana = maxMana;
	}

    IEnumerator CoolDownDmg()
    {
        onCD = true;
        yield return new WaitForSeconds(coolDown);
        onCD = false;
    }

	
	// Update is called once per frame
	void Update () {
	   
	}

    private void HandleMana()
    {
        float currentXScale = (float)currentMana / (float) maxMana;
        //healthTransform.position = new Vector3(currentXvalue, cachedY);
        //manaTransform.localScale = new Vector3(manaTransform.localScale.x * currentXScale, manaTransform.localScale.y);
/*
        if(CurrentHealth > maxHealth / 2) //plus de 50% de vie
        {
            visualHealth.color = new Color32((byte)MapValues(CurrentHealth, maxHealth / 2, maxHealth, 255, 0), 255, 0, 255);
        }
        else //moins de 50%
        {
            visualHealth.color = new Color32(255, (byte)MapValues(CurrentHealth,0, maxHealth / 2, 0, 255), 0, 255);
        }
        */
    }

    private float MapValues(float x, float inMin, float inMax, float outMin, float outMax)
    {
        return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }

    public void TakeMana(int ammount)
    {
        if (ammount > CurrentMana)
        {
            ammount = CurrentMana;
        }


        if (!onCD && currentMana > 0)
        {
            StartCoroutine(CoolDownDmg());
            CurrentMana -= ammount;
        }

    }
    public void GetMana(int ammount)
    {
        
        if (!onCD && currentMana >= 0 && currentMana < maxMana)
        {
            StartCoroutine(CoolDownDmg());
            CurrentMana += ammount;
        }

    }
}
