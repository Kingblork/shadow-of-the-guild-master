﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

	private bool attacking = false;
	private float attackTimer = 0;
	private float attackCD = 1f;
	private float comboDelay = 0.3f;
	private bool hit2 = true;
	private bool hit3 = false;


	public Collider2D triggerAttack;
	public Collider2D sphereAttack;

	private Animator anim;
	private Behaviour halo;


	void Awake(){
		//Récupération du composant d'animation (pour pouvoir jouer les animations de mon choix)
		anim = gameObject.GetComponent<Animator> ();
		//Les triggers d'attaque sont désactivés
		triggerAttack.enabled = false;
		sphereAttack.enabled = false;
		halo = (Behaviour)GetComponent("Halo");
		halo.enabled = false;
	}

	void Update(){
			//Si on presse ctrl gauche et qu'on est pas déjà entrain d'attaquer
			if(Input.GetKeyDown("left ctrl")){
			if(!attacking){
					//On passe la variable d'attaque à true
					attacking = true;
					attackTimer = attackCD;
					//Si le timer d'attack vient d'etre initialisé avec la durée de l'attaque le trigger
					//est activé et permet de déclancher la frappe
					if(attackTimer == attackCD){
						//triggerAttack.enabled = true;
						//Jouer l'animation de frappe
						anim.Play("hit1");
					}
				}else{
					if(Input.GetKeyDown("left ctrl") && (attackTimer > 0 && attackTimer<attackCD) && (attackTimer>comboDelay))
					{
						//triggerAttack.enabled = false;
						//triggerAttack.enabled = true;

						if(hit2 == true){
							anim.CrossFade("hit2",0.3f);
							hit2=false;
							hit3=true;
						}else{
							if(hit3==true){
								anim.CrossFade("hit3",0.5f);
								hit3=false;
							}
						}
					}
				}
			}

			if (Input.GetKeyDown ("w") && !attacking) {
				//On passe la variable d'attaque à true
				attacking = true;
				attackTimer = attackCD;
				//Si le timer d'attack vient d'etre initialisé avec la durée de l'attaque le trigger
				//est activé et permet de déclancher la frappe
				if(attackTimer == attackCD){
					sphereAttack.enabled = true;
					//Jouer l'animation de frappe
					anim.Play("BouclierEnergie");
					halo.enabled = true;
				}
			}

			//Si le timer d'attack est supérieur à 0 on le décrémente 
			if (attackTimer > 0) {
				attackTimer -= Time.deltaTime;
			} else {
				//Sinon on considère l'attaque terminer et on désactive les trigger d'attaque
				attacking=false;
				triggerAttack.enabled = false;
				sphereAttack.enabled = false;
				halo.enabled = false;
				hit2 =true;
				hit3=false;
			}
	}
}
