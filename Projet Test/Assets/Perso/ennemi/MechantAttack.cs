﻿using UnityEngine;
using System.Collections;

public class MechantAttack : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D col){
		if(col.isTrigger == false && col.CompareTag("Perso")){
			col.SendMessageUpwards("Dommage",20);
		}
	}
}
