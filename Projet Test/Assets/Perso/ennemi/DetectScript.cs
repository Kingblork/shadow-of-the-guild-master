﻿using UnityEngine;
using System.Collections;

public class DetectScript : MonoBehaviour {

	private MechantScript[] scriptParent;

	void Awake (){
		scriptParent = this.GetComponentsInParent<MechantScript>();
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.isTrigger == false && col.CompareTag("Perso")){
			scriptParent[0].detect=true;
		}
	}

	void OnTriggerExit2D(Collider2D col){
			scriptParent[0].detect=false;

	}

	
}