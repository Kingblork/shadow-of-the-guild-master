﻿using UnityEngine;
using System.Collections;

public class EnnemiAI : MonoBehaviour {

	public int health = 100;
	private Animator anim;
	private int delay = 2; //

	// Use this for initialization
	void Start () {
		//Récupération de l'animator
		anim = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		//Si la vie est inférieur ou égale à 0
		if (health <= 0) {
			//Appel de la fonction de destruction de l'objet
			anim.Play("Destroy");
			GameObject.Destroy(gameObject,1);

		}
	}

	public void Dommage(int dmg){
		health -= dmg;
		anim.Play("hit");
	}

	
}
