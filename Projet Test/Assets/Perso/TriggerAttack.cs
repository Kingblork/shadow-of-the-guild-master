﻿using UnityEngine;
using System.Collections;

public class TriggerAttack : MonoBehaviour {
	
	public int dmg = 20;
	public int health = 100;
	
	void OnTriggerEnter2D(Collider2D col){
		if(col.isTrigger == false && col.CompareTag("Ennemi")){
			col.SendMessageUpwards("Dommage",dmg);
		}
	}
	
	
	
}
